Name: helloworld
Version: 1
Release: 1%{?dist}
Summary: Hello World

License: GPL
Source0: %{name}-%{version}.tar.gz

BuildArch: noarch

BuildRequires: make
Requires: bash

%description
Hello World and hello word.


%prep
%setup -q


%build
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
%make_install


%files
%{_bindir}/%{name}
%config %{_sysconfdir}/%{name}.conf
%doc README
%license COPYING


%changelog
